<?php
require_once './vendor/autoload.php';
require_once './User.php';

use Symfony\Component\Validator\Constraints\{Length, NotBlank};
use Symfony\Component\Validator\Validation;

$validator = Validation::createValidator();

$arr = array("foo", "bar", "hallo_word",);

for ($i = 0; $i < count($arr); ++$i) {
	$s = $arr[$i];
	echo "Entered '$s' string." . '<br>';

	$violations = $validator->validate($s, [
		new Length(['min' => 10]),
		new NotBlank(),
	]);

	if (0 !== count($violations)) {
		// there are errors, now you can show them
		foreach ($violations as $violation) {
			echo $violation->getMessage() . '<br>';
		}
	} else {
		echo "Good string." . "<br>";
	}
}

//Wrong user.
$u1 = new User('a', 'name', 'pass');
$u1->echoPrint();
echo $u1->getCunstructDate() . '<br>';

//Good user.
$us2 = new User('AB-SD1-32453', 'good@miet.ru', 'aA1#1234');
$us2->echoPrint();
echo $us2->getCunstructDate() . '<br>';
